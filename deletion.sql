-- ::::::::::::::::::::::::::
--  On choisi la bdd
-- ::::::::::::::::::::::::::

use  tpCompagnie;


-- ::::::::::::::::::::::::::
--  suppression des tables
-- ::::::::::::::::::::::::::


DROP TABLE affectation;

DROP TABLE pilote;

DROP TABLE vol;

DROP TABLE avion;

DROP TABLE typeAv;

DROP TABLE constructeur;

DROP TABLE aeroport;