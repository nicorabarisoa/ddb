-- ::::::::::::::::::::::::::
--  CREATION DE LA BDD 
-- ::::::::::::::::::::::::::

CREATE DATABASE if not exists tpCompagnie;

-- ::::::::::::::::::::::::::
--  On choisi la bdd
-- ::::::::::::::::::::::::::

use  tpCompagnie;

-- ::::::::::::::::::::::::::
--  création des tables
-- ::::::::::::::::::::::::::

CREATE TABLE if not exists aeroport (
    IdAeroport varchar(3) PRIMARY KEY,
    NomAeroport varchar(15) NOT NULL,
    NomVilleDesservie varchar(15),
    CONSTRAINT CHK_Person CHECK (IdAeroport not like '%[0-9]%')
);

CREATE TABLE if not exists constructeur (
    idConstructeur int AUTO_INCREMENT PRIMARY KEY NOT NULL,
	NomConstructeur varchar(15) NOT NULL 
);



CREATE TABLE if not exists typeAv (
    TypeAvion varchar(10) NOT NULL PRIMARY KEY,
	Capacite int NOT NULL ,
    idConstructeur int NOT NULL ,
    FOREIGN KEY (idConstructeur) REFERENCES constructeur(idConstructeur),
    CHECK (Capacite>=50 and capacite <= 100),
    check ((TypeAvion) NOT LIKE '%[0-9]%')
);
ALTER TABLE typeAv
ALTER Capacite SET DEFAULT 100;

CREATE TABLE if not exists avion (
    NumAvion int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    TypeAvion varchar(10) NOT NULL,
    BaseAeroport varchar(3) ,
    FOREIGN KEY (BaseAeroport) REFERENCES aeroport(IdAeroport),
    FOREIGN KEY (TypeAvion) REFERENCES typeAv(TypeAvion)
);
ALTER TABLE avion AUTO_INCREMENT = 100;






CREATE TABLE if not exists vol (
    NumVol varchar(5) NOT NULL PRIMARY KEY,
	AeroportDept varchar(3) NOT NULL,
    Hdepart TIME (0) NOT NULL,
    AeroportArr varchar(3) NOT NULL ,
    Harrivee TIME (0) NOT NULL,
    FOREIGN KEY (AeroportDept) REFERENCES aeroport(IdAeroport),
    FOREIGN KEY ( AeroportArr) REFERENCES aeroport(IdAeroport),
    check ( NumVol like 'IT[0-9][0-9][0-9]')
    
);

CREATE TABLE if not exists pilote (
    IdPilote int  AUTO_INCREMENT NOT NULL PRIMARY KEY  ,
	NomPilote varchar(15) NOT NULL,
    PrenomPilote varchar(15) NOT NULL ,
    CONSTRAINT pi_nomPnom UNIQUE (NomPilote,PrenomPilote)
);


CREATE TABLE if not exists affectation (
    NumVol varchar(5) NOT NULL ,
	DateVol date NOT NULL PRIMARY KEY,
    NumAvion int not null,
    IdPilote int ,
    FOREIGN KEY (NumVol) REFERENCES vol(NumVol),
    FOREIGN KEY ( NumAvion) REFERENCES avion(NumAvion),
    FOREIGN KEY ( IdPilote) REFERENCES pilote(IdPilote)
);



INSERT INTO constructeur (nomConstructeur)
VALUES ('aaaddddzaaa');
INSERT INTO constructeur (nomConstructeur)
VALUES ('aaazddddzaaa');

INSERT INTO aeroport (IdAeroport,NomAeroport,NomVilleDesservie)
VALUES ('B8S','Poeretta','Bastia');






select *
from constructeur;





